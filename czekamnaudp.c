#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "err.h"
#include <endian.h>
#include <inttypes.h>
#define MICRO 1000000

int main(int argc, char *argv[])
{
    int sock, flags, sflags = 0;
    struct sockaddr_in servAddr, clientAddr;
    socklen_t sent, recv;
    ssize_t len, sentLen;
    uint64_t mesgIn, mesgOut[2];
    short isBigEndian;
    struct timeval ans;

    if (argc != 2)
        fatal("Usage: %s [port numer]\n", argv[0]);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(atoi(argv[1]));
    if (bind(sock, (struct sockaddr *) &servAddr,
                (socklen_t) sizeof(servAddr)) < 0)
        syserr("bind");

    sentLen = (socklen_t) sizeof(clientAddr);
    while (1)
    {
        do
        {
            recv = (socklen_t) sizeof(clientAddr);
            flags = 0;
            len = recvfrom(sock, &mesgIn, sizeof(mesgIn), flags,
                    (struct sockaddr *) &clientAddr, &recv);
            gettimeofday(&ans, NULL);
            if (len < 0)
                syserr("recvfrom");
            else
            {
                mesgOut[0] = mesgIn; //allready bigendian
                mesgOut[1] = (uint64_t) (ans.tv_sec * MICRO) + ans.tv_usec;
                mesgOut[1] = htobe64(mesgOut[1]);
                sent = sendto(sock, mesgOut, sizeof(mesgOut), sflags, 
                        (struct sockaddr *) &clientAddr, sentLen);
                if (sent != sizeof(mesgOut))
                        syserr("sendto");
            }
        }
        while (len > 0);
    }
    if (close(sock) == -1)
        syserr("sock");
    return 0;
}
