TARGET: ileczekam czekamnaudp

CC	= gcc
FLAGS	= -Wall


ileczekam: ileczekam.o err.o
	$(CC) $(FLAGS) $^ -o $@

czekamnaudp: czekamnaudp.o err.o
	$(CC) $(FLAGS) $^ -o $@

.PHONY: clean TARGET
clean:
	rm -f ileczekam czekamnaudp *.o *~ *.bak
