#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <stdint.h>
#include <inttypes.h>
#include <endian.h>
#include "err.h"

#define MICRO 1000000

void tcpLag(struct addrinfo *hints, struct addrinfo **result, char **args)
{
    int err, sock;
    struct timeval tvBefore, tvAfter;

    hints->ai_socktype = SOCK_STREAM;
    hints->ai_protocol = IPPROTO_TCP;
    
    err = getaddrinfo(args[2], args[3], hints, result);
    if (err != 0)
        syserr("getaddrinfo: %s\n", gai_strerror(err));

    sock = socket((*result)->ai_family, (*result)->ai_socktype,
            (*result)->ai_protocol);
    if (sock < 0)
        syserr("socket");

    //check time diff before and after connection
    gettimeofday(&tvBefore, NULL);
    err = connect(sock, (*result)->ai_addr, (*result)->ai_addrlen);
    gettimeofday(&tvAfter, NULL);

    freeaddrinfo(*result);

    printf("Time connecting: %lu microseconds\n", 
            ((tvAfter.tv_sec * MICRO) + tvAfter.tv_usec - 
            ((tvBefore.tv_sec * MICRO) + tvBefore.tv_usec)));

    if (err < 0) //print how it took to connect even if there was an error
        syserr("connect");

    if (close(sock) == -1)
        syserr("close");
}

void udpLag(struct addrinfo *hints, struct addrinfo **result, char **args)
{
    int err, sock, flags;
    struct sockaddr_in myAddr, srvAddr;
    ssize_t sent, recv, len;
    uint64_t mesg, ans[2];
    short isBigEndian;
    struct timeval tvBefore;
    socklen_t rcva;

    hints->ai_family = AF_INET;
    hints->ai_socktype = SOCK_DGRAM;
    hints->ai_protocol = IPPROTO_UDP;
    hints->ai_flags = 0;
    hints->ai_addrlen = 0;
    hints->ai_addr = NULL;
    hints->ai_canonname = NULL;
    hints->ai_next = NULL;
    
    err = getaddrinfo(args[2], NULL, hints, result);
    if (err != 0)
        syserr("getaddrinfo: %s\n", gai_strerror(err));

    myAddr.sin_family = AF_INET;
    myAddr.sin_addr.s_addr = 
        ((struct sockaddr_in*) ((*result)->ai_addr))->sin_addr.s_addr;
    myAddr.sin_port = htons((uint16_t) atoi(args[3]));
    freeaddrinfo(*result);
    
    sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    flags = 0;
    recv = (socklen_t) sizeof(myAddr);
    len = sizeof(uint64_t);

    gettimeofday(&tvBefore, NULL);

    mesg = (uint64_t) (tvBefore.tv_sec * MICRO) + tvBefore.tv_usec;

    mesg = htobe64(mesg); //turn current time to big endian

    sent = sendto(sock, &mesg, len, flags, (struct sockaddr *) &myAddr, recv);
    if (sent != (int) len)
        syserr("sendto");

    flags = 0;
    len = (size_t) sizeof(ans);
    rcva = (socklen_t) sizeof(srvAddr);
    recv = recvfrom(sock, ans, len, flags, (struct sockaddr *) &srvAddr,
            &rcva); //get time when server sent his answer
    if (recv < 0)
        syserr("recvfrom");
    if (recv != sizeof(uint64_t) * 2)
        fatal("Error in answer");
    
    //print to stderr what data was received
    fprintf(stderr, "%" PRIu64 " %" PRIu64 "\n", ans[0], ans[1]);

    //transform received data to our eniandess
    ans[0] = be64toh(ans[0]);
    ans[1] = be64toh(ans[1]);
   
    printf("%" PRIu64 "\n", ans[1] - ans[0]); 
    if (close(sock) == -1)
        syserr("close");
}

void usage(char * name)
{
    fatal("Usage: %s [type (-t/-u)] [adress] [port]", name);
}

int main(int argc, char *argv[])
{
    if (argc != 4)
        usage(argv[0]);
    
    short checkTCP = strcmp(argv[1], "-t");
    short checkUDP = strcmp(argv[1], "-u"); 

    struct addrinfo addrHints;
    struct addrinfo *addrResult;

    memset(&addrHints, 0, sizeof(struct addrinfo));
    addrHints.ai_family = AF_INET; //IPv4

    if (checkTCP == 0)
        tcpLag(&addrHints, &addrResult, argv);
    else if (checkUDP == 0)
        udpLag(&addrHints, &addrResult, argv);
    else
       usage(argv[0]); 

    return 0;
}
